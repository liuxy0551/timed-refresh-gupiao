# timed-refresh

## 定时刷新服务

## 项目开发

使用 `yarn` 进行包管理

```sh
git clone https://gitee.com/liuxy0551/timed-refresh-gupiao.git
cd timed-refresh-gupiao
yarn
```

### 开发

```sh
yarn dev
```

### 部署

```sh
s deploy
```

&emsp;&emsp;本服务通过阿里云的函数计算部署，属于 Serverless 的一种实现，具体可点击 https://www.aliyun.com/product/fc 查看。项目下的 `s.yaml` 为部署的配置文件，部署工具为 [Serverless Devs](https://help.aliyun.com/zh/fc/developer-reference/serverless-devs/)。

## 注意事项

&emsp;&emsp;每次新的服务需要定时刷新时，需要修改以下内容：

-   `index.js` 文件中 `url` 的值
-   `s.yaml` 文件中 `functionName` 的值
-   `s.yaml` 文件中 `access` 的值是部署工具 `s` 本地配置的别名
